// section3 tabs //

// function openServise(event, desing){
//     let i, tabcontent, tablinks;
//     tabcontent = document.getElementsByClassName("tabcontent");
//     for (i = 0; i <tabcontent.length; i++) {
//         tabcontent[i].style.display = "none";
//     }
//     tablinks = document.getElementsByClassName("tablinks");
//     for (i=0; i<tabcontent.length; i++){
//         tablinks[i].className = tablinks[i].className.replace("active","");
//     }
//     document.getElementById(desing).style.display = "block";
//     event.currentTarget.className += "active";
// }
// document.getElementById("defaultOpen").click();

/////////////////////////////////////////////////
/// tabs, section servises

let tabs = document.querySelectorAll('[id]');
let active = document.getElementsByClassName('design');

const toggleContent = function() {
    if (!this.classList.contains("design-link")) {
        Array.from(active).forEach( item => {
            item.classList.remove('design');
        });

        let tabDataValue = this.getAttribute('designOpen');
        let tabContent = document.querySelector(`[data-content=${tabDataValue}]`);

        this.classList.add('design');
        tabContent.classList.add('design');
    }
};

Array.from(tabs).forEach( item => {
    item.addEventListener('click', toggleContent);
});







/////////////////////////////////////////////////
let photo = document.querySelectorAll('[data-foto]');
let activePhoto = document.getElementsByClassName('active1');

const photoContent = function() {
    if (!this.classList.contains("ux-designer")) {
        Array.from(activePhoto).forEach( item => {
            item.classList.remove('ux-designer');
        });

        let photoDataValue = this.getAttribute('data-foto');
        let photoContent = document.querySelector(`[data-box=${photoDataValue}]`);

        this.classList.add('active1');
        photoContent.classList.add('active1');
    }
};

Array.from(photo).forEach( item => {
    item.addEventListener('click', fotoContent);
});



let buttonLeft = document.querySelector(".btn-preview.back")
let buttonRight = document.querySelector(".btn-preview.forward")
let activeSlide = document.getElementsByClassName('.')
let slidesCount = photo.length


buttonLeft.addEventListener('click', function(){
    let numValue = activeSlide[0].getAttribute('data-num');
    let prevIndex = numValue -1

    if (prevIndex < 0){
        prevIndex = slidesCount -1
    }

    let prevSlide = document.querySelector(`[data-num='${prevIndex}']`)

    prevSlide.click()
}) ;

buttonRight.addEventListener('click', function(){
    let numValue = +activeSlide[0].getAttribute('data-num');
    let nextIndex = (numValue +1)%slidesCount
    let nextSlide= document.querySelector(`[data-num='${nextIndex}']`)

    nextSlide.click()
}) ;



let workBoxTabs = document.querySelectorAll('[data-workTabs]');
let activeWork = document.getElementsByClassName('active3');
let workBoxFoto = document.getElementsByClassName('work_foto_item');


const fillterContent=function(){

    if (!this.classList.contains("active3")) {
        Array.from(activeWork).forEach( item => {
            item.classList.remove('active3');
        });
        this.classList.add('active3');

        Array.from(workBoxFoto).forEach(item => {
            item.classList.add('hide')
        });

        let tabsValue = this.getAttribute('data-workTabs');

        if (tabsValue == ('all')){
            Array.from(workBoxFoto).forEach(item => {
                item.classList.remove('hide')
            });
        }else{
            let boxFotoValue = document.querySelectorAll(`[data-workFoto=${tabsValue}]`);

            Array.from(boxFotoValue).forEach(item =>{
                item.classList.remove('hide')
            })
        }

    }



};
Array.from(workBoxTabs).forEach( item => {
    item.addEventListener('click', fillterContent);
});


let buttonClick = document.querySelector('.workButton')
let countForOpen = 12

buttonClick.addEventListener('click', function(){
    let hiddenGallery =document.querySelectorAll('.work_foto .dnone')

    for( let i=0; i<countForOpen; i++){
        if(hiddenGallery[i] == undefined){
            buttonClick.classList.add('hide')
            break
        }else{
            hiddenGallery[i].classList.remove('dnone')
        }
    }

    if (hiddenGallery.length == countForOpen){
        buttonClick.classList.add('hide')
    }
})