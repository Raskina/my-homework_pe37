//Домашка 8

const price = document.createElement("input");
const p = document.createElement("p");
p.textContent = "price";
p.append(price);
document.body.prepend(p);

price.addEventListener("focus", function (event){
    event.target.style.cssText = "outline: 2px solid green";
});
price.addEventListener("blur", function (event){
   const priceValue = price.value;
   if (priceValue < 0){
       let textError = document.createElement("p");
       textError.textContent = "Please enter correct price";
       p.after(textError);
       event.target.style.cssText = "outline: 2px solid red";
   } else {
       const par = document.createElement("p");
       const span = document.createElement("span");
       span.textContent = `Текущая цена: ${priceValue}`;
       const btn = document.createElement("button");
       btn.textContent = "X";
       par.append(span, btn);
       p.before(par);
       btn.addEventListener("click", function (event){
           par.remove();
           price.value = "";
       })
   }

    // event.target.style.cssText = "outline: 2px solid transparent";
});