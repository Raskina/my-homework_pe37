// /Домашка 7

function showList(mas, parent = document.body) {
    let list = document.createElement("ul");
    console.log(list)
    let listItems = mas.map(function (el){
        if (el instanceof Array){
            let innerList = '<ul>';

           let innerListItems = el.map(function (el){
                return `<li>${el}</li>`
            });
            innerListItems.forEach(function (el) {
                innerList += el;
            })
            return "<li>"+innerList+"</ul></li>";

        }
        return `<li>${el}</li>`
    });
    listItems.forEach(function (el) {
        list.innerHTML += el;
    })
    parent.prepend(list);

    let timerText = document.createElement("p");
    timerText.innerText = "3";
    list.after(timerText);

    let interval = setInterval(function (){
        timerText.innerText = timerText.innerText - 1;

        if (timerText.innerText === "0"){
            clearInterval(interval);
        }
    }, 1000);


    let timer = setTimeout(function (){
        list.remove();
        timerText.remove();
        clearTimeout(timer);
    }, 3000);
}
showList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"])