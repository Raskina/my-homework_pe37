//Домашка 6


function filterBy(array, datatype) {
    return array.filter(function (el){
        if (typeof el === "object"){
            return !(el instanceof Object);
        }
        return typeof el !== datatype;
    })
}
console.log(filterBy(['hello', 'world', 23, '23', null, {}], 'object'))