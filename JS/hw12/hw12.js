// Домашка 12

const images = [...document.querySelectorAll(".image-to-show")];
let interval;
function playSlider() {
    interval = setInterval(function () {
        const carentImage = document.querySelector(".image-to-show.active");
        let currIndex = images.indexOf(carentImage);
        if (currIndex === images.length - 1) {
            currIndex = 0
        } else {
            currIndex++;
        }
        carentImage.classList.remove("active");
        images[currIndex].classList.add("active")
    }, 3000);
}
playSlider();
const btnStop = document.querySelector(".stop");
btnStop.addEventListener('click', function (event) {
    clearInterval(interval)
})
const btnPlay = document.querySelector(".play");
btnPlay.addEventListener('click', function (event) {
    playSlider()
});