// Домашка 11


window.addEventListener("keypress", event => {
    console.log(event.key)
    const btns = [...document.querySelectorAll(".btn")];
    const oldPressedBtn = document.querySelector(".btn-pressed");
    if (oldPressedBtn){
        oldPressedBtn.classList.remove("btn-pressed")
    }
    const pressedBtn = btns.find(el => el.textContent.toLowerCase() === event.key.toLowerCase());
    pressedBtn.classList.add("btn-pressed");
})