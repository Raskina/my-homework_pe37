//Домашка 5 (HW5)

function createNewUser() {
    let firstname = prompt("Your first Name?");
    let lastname = prompt("Your last Name?");
    let birthday = prompt("Your birthday: dd.mm.yyyy")

    const newUser = {
        firstname,
        lastname,
        getAge(){
            let rightDateFormat = birthday.split(".").reverse().join(".");
            return Math.floor((Date.now() - new Date(rightDateFormat)) / (1000 * 60 * 60 * 24 * 365));
        },

        getLogin() {
            return firstname[0].toUpperCase() + lastname.toLowerCase();
        }
    }
    return newUser
}

const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());