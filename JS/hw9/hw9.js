// Домашка 9

const tabs = document.querySelector(".tabs")
tabs.addEventListener('click', (event) => {
    const target = event.target.closest(".tabs-title");
    if (target){
        const tabsTitleActive = document.querySelector(".tabs-title.active");
        tabsTitleActive.classList.remove("active");

        target.classList.add("active");

        const activeTabContent = document.querySelector(".tabs-content li.active");
        activeTabContent.classList.remove("active");

        const targetTabContent = document.querySelector(`.tabs-content [data-target="${target.dataset.target}"]`);
        targetTabContent.classList.add("active");
    }
})